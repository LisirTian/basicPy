import socket
import threading

ip_prefix="192.168.1."

def scan(ip):
    try:
        socket.setdefaulttimeout(1)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(ip,135) # 尝试连接主机135端口
        print("Host %s is Up" % ip)
    except:
        print("error")
    # finally:
        # s.close()

def get_host_name_ip(ip):
    try:
        hostName = socket.gethostbyaddr(ip)
        print("Hostname: %s, IP: %s" % hostName[0],ip)
    except socket.herror:
        print("error")

if __name__ == '__main__':
    for i in range(1,255):
        ip = ip_prefix+ str(i)
        print(ip)
        # threading.Thread(target=scan, args=(ip,)).start()
        scan(ip)