# coding:UTF-8
import psutil
import multiprocessing, time  # 导入多进程开发模块


def process_iter(msg):
    for process in psutil.process_iter():  # 获取全部进程

        print("[]进程ID: %s, 进程名称: %s, 创建时间 %s" % (
            process.pid,
            process.name(),
            process.create_time()))

def cpu():
    print("物理CPU数量%d" % psutil.cpu_count(logical=False))
    print("逻辑CPU数量%d" % psutil.cpu_count(logical=True))
    print("用户CPU使用时间:%f, 系统CPU使用时间:%f, CPU空闲时间:%s" % (
        psutil.cpu_times().user, psutil.cpu_times().system, psutil.cpu_times().idle
    ))
    for x in range(10):
        print("CPU使用率监控:%s" % psutil.cpu_percent(interval=1, percpu=True))

def disk():
    print(psutil.disk_usage("d:"))

def main():
    disk()

if __name__ == '__main__':
    main()
