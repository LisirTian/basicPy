# coding:UTF-8

import multiprocessing, time  # 导入多进程开发模块


# print("CPU内核数量: %s" % multiprocessing.cpu_count()) # CPU可用数量

def worker(item):
    time.sleep(1)
    return "进程ID: %s, 进程名称: %s, item = %s" % (

            multiprocessing.current_process().pid,
            multiprocessing.current_process().name, item)


def func2():
    pool = multiprocessing.Pool(processes=2)
    for item in range(10):
        result = pool.apply_async(func=worker, args=(item,))
        print(result.get())
    pool.close()
    pool.join()




def main():
    func2()
    pass


if __name__ == '__main__':
    main()
