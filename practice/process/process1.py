# coding:UTF-8

import multiprocessing, time  # 导入多进程开发模块


# print("CPU内核数量: %s" % multiprocessing.cpu_count()) # CPU可用数量

def worker(delay, count):
    for num in range(count):
        print("[%s]进程ID: %s, 进程名称: %s" % (
            num,
            multiprocessing.current_process().pid,
            multiprocessing.current_process().name))
        time.sleep(delay)


def func1():
    for item in range(3):
        process = multiprocessing.Process(target=worker, args=(1, 10,), name="site进程-%s" % item)
        process.start()


def func2():
    for item in range(3):
        process = MyProcess(name="site进程-%s" % item, delay=1, count=10)
        process.start()

    # print("进程ID:%s, 进程名称:%s" % (multiprocessing.current_process().pid,
    #                             multiprocessing.current_process().name)) #主函数进程信息


class MyProcess(multiprocessing.Process):
    def __init__(self, name, delay, count):
        super().__init__(name=name)
        self.__delay = delay
        self.__count = count

    def run(self):
        for num in range(self.__count):
            print("[%s]进程ID: %s, 进程名称: %s" % (
                num,
                multiprocessing.current_process().pid,
                multiprocessing.current_process().name))
            time.sleep(self.__delay)


def main():
    func2()
    pass


if __name__ == '__main__':
    main()
