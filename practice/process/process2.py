# coding:UTF-8

import multiprocessing, time  # 导入多进程开发模块


def send(msg):
    time.sleep(10)
    print("[]进程ID: %s, 进程名称: %s" % (
        multiprocessing.current_process().pid,
        multiprocessing.current_process().name))



def main():
    process = multiprocessing.Process(target=send, args=("basicpy",), name="发送进程")
    process.start()
    # process.join()
    time.sleep(2)
    if process.is_alive():
        process.terminate()
        print("%s 进程中断..." % process.name)
    print("[]进程ID: %s, 进程名称: %s 进程执行完毕..." % (
        multiprocessing.current_process().pid,
        multiprocessing.current_process().name))

if __name__ == '__main__':
    main()
