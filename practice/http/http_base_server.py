import socket
import multiprocessing

class HTTPServer:
    def __init__(self, port):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # 创建socket实例
        self.server_socket.bind(("0.0.0.0",port))
        self.server_socket.listen()
    def start(self): # 服务器开始提供服务
        while True:
            client_socket, client_address = self.server_socket.accept()
            print("[新的客户端连接]客户端ip: %s,访问端口: %s"% client_address)
            handle_client_process= multiprocessing.Process(target=self.handle_response, args=(client_socket,))
            handle_client_process.start()

    def handle_response(self, client_socket):
        request_headers= client_socket.recv(1024)
        print("客户端请求头信息:%s"% request_headers.decode())
        response_start_line= "HTTP/1.1 200 OK \r\n"
        request_headers="Server:basic Server \r\n Content-Type:text/html\r\n\r\n"
        response_body="<html>" \
                      "<head>" \
                      "     <title>basicpy</title>" \
                      "<meta charset='UTF-8'></meta>" \
                      "</head>" \
                      "<body>" \
                        "<h1>basic"\
                        "</h1>"\
                        "<h1>basic http"\
                        "</h1>"\
                      "</body>" \
                      "</html>"
        response = response_start_line+ request_headers+ response_body
        client_socket.send(bytes(response,"UTF-8")) #服务器响应
        client_socket.close()





def main():
    http_server = HTTPServer(80)
    http_server.start()


if __name__ == '__main__':
    main()