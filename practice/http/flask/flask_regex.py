import werkzeug.routing, flask # pip install flask
class RegexConverter(werkzeug.routing.BaseConverter):
    def __init__(self, url_map, *args):
        super(RegexConverter,self).__init__(url_map)
        self.regex = args[0]
app = flask.Flask(__name__) # 创建flask的核心对象,所有的处理都基于此对象

app.url_map.converters["regex"] = RegexConverter # 注册转换器
# 转换器 string path int float uuid
@app.route("/<regex('\d{3,6}'):nid>", methods=["GET","POST"])
def index(nid):
    json = [{"nid":nid},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

def main():
    app.run(host="0.0.0.0", port=80)

if __name__ == '__main__':
    main()