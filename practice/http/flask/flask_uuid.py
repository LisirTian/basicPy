import flask # pip install flask
import uuid
app = flask.Flask(__name__) # 创建flask的核心对象,所有的处理都基于此对象

print(uuid.uuid1())
# http://localhost/09494b20-408d-11ec-bad4-1cbfce80561e
# 转换器 string path int float uuid
@app.route("/<uuid:photo_name>", methods=["GET","POST"])
def index(photo_name):
    json = [{"photo_name":photo_name},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

def main():
    app.run(host="0.0.0.0", port=80)

if __name__ == '__main__':
    main()