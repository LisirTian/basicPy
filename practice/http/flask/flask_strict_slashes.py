import flask # pip install flask

app = flask.Flask(__name__) # 创建flask的核心对象,所有的处理都基于此对象


@app.route("/message/", strict_slashes=True)
def index():
    json = [{"ee":"message"},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

@app.route("/teacher", strict_slashes=False)
def index2():
    json = [{"ee":"teacher"},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

def main():
    app.run(host="0.0.0.0", port=80)

if __name__ == '__main__':
    main()