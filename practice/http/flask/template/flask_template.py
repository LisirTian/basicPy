import flask # pip install flask

app = flask.Flask(__name__) # 创建flask的核心对象,所有的处理都基于此对象


@app.route("/")
def index():
    return flask.render_template("index.html",msg="message", name="Li")

def main():
    app.run(host="0.0.0.0", port=80, debug=True)

if __name__ == '__main__':
    main()