import flask # pip install flask

app = flask.Flask(__name__) # 创建flask的核心对象,所有的处理都基于此对象

# 子域名示例
@app.route("/", subdomain="mes", methods=["GET","POST"])
def message():
    json = [{"ee": "message"}, {"aa": "aaa", "bb": [1, 2, 3, 4, 5]}]
    return flask.jsonify(json)

@app.route("/",subdomain="tea", methods=["GET","POST"])
def teacher():
    json = [{"ee":"teacher"},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

def main():
    app.run(host="0.0.0.0", port=80,)

if __name__ == '__main__':
    main()