import flask # pip install flask

app = flask.Flask(__name__)



def index():
    print("反向路由: %s" % flask.url_for("index"))
    json = [{"ee":"ddds"},{"aa":"aaa","bb":[1,2,3,4,5]}]
    return  flask.jsonify(json)

def main():
    # endpoint 路由的唯一标记, 默认是函数名称
    app.add_url_rule(rule="/", endpoint="index", view_func=index, methods=["GET","POST"])
    app.run(host="0.0.0.0", port=80)

if __name__ == '__main__':
    main()