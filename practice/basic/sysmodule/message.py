'''
包  模块(*.py)

import 包 模块名称 [as 别名], 包 模块名称 [as 别名], ...
from 包.模块 import 结构名称 [as 别名], 结构名称 [as 别名], ...
'''

# import com.practice.basic.practice.compileFunc as compile

#import com.practice.basic.practice.func4 as fun
from practice.basic.function import factorial as fac


# fun.print_num()

print(fac(13))
print(dir())