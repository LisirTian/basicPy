# coding:UTF-8
# 数据处理

numbers= [1,2,3,4,5,6,7,8,9]
filter_result = list(filter(lambda item : item%2 ==0,numbers))
print("filter_result: %s" %filter_result)
map_result = list(map(lambda item : item*2,filter_result))
from functools import reduce
print(map_result)
reduce_result = reduce(lambda x,y:x+y,map_result)
print(reduce_result)