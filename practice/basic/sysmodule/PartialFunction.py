from functools import partial

def add(a,b,c=2):
    return a+b+c

print(add(100,200))
print(add(100,200,30))


plus = partial(add,100,200) # 此时a和b的内容就确定了

print(plus()) # c使用默认值
print(plus(30)) # c的值为30