import copy

# 浅拷贝和深拷贝

dict1 = {"aa": [1, 2, 3, {"tt":[34,2,4432]}], "bb": "bbb", "cc": 232343}
# dict2 = dict1.copy()
dict2 = copy.deepcopy(dict1)
print(dict1)
print(dict2)

print("=====")
dict2['cc'] = '343'
print(dict1)
print(dict2)

print("=====")
dict2["aa"][0]="ss"
dict2["aa"][1]=45
dict2["aa"][3]["tt"][0]=545454
print(dict1)
print(dict2)
