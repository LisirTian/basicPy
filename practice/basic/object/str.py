class Message():
    def __init__(self, content):
        self.__content = content

    def __str__(self):
        return "__str__()】 %s" % self.__content

    # __repr__ = __str__ # 可以引用

    def __repr__(self): # 开发调试时使用的
        return "__repr__()】 %s" % self.__content

    def __lt__(self, other):
        pass


def main():
    msg = Message("practice")
    print(msg.__str__())
    print(str(msg))

    print(repr(msg))

if __name__ == '__main__':
    main()

