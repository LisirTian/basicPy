"""
__new__()构造器

def __init__(self): 是在 new后面调用的

__new__是在实例创建之前被调用的，因为它的任务就是创建实例然后返回该实例对象，是个静态方法。
__init__是当实例对象创建完成后被调用的，然后设置对象属性的一些初始值，通常用在初始化一个类实例的时候。是一个实例方法。
self不是关键字, 可用this或其他代替
"""

class Message:
    def __new__(cls, *args, **kwargs):
        print("class = %s, args = %s, kwargs = %s" %(cls, args, kwargs ))
        return object.__new__(cls) # 继续执行本类的init()方法执行
    def __init__(self, **kwargs):
        print("init kwarges = %s" % kwargs)
    def setInfo(self,name):
        self.name=name

def main():
    msg = Message(title="practice", content = "content")
    print(str(msg))
    print(__name__)

if __name__ == '__main__':
    main()

