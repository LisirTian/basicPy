class Person:
    def __init__(self):
        self.name=None
        self.age=0
    def setAge(self,age):
        self.age=age

    def setName(self,name):
        self.name=name
    def getName(self):
        return self.name
    def getAge(self):
        return self.age

class Student(Person):
    def __init__(self):
        self.school=None
    def setSchool(self, school):
        self.school=school
    def getSchool(self):
        return self.school

def main():

    stu= Student()
    stu.setName("ken")
    stu.setAge(17)

    print("姓名:%s, 年龄:%s, 学校:%s" % (stu.getName(), stu.getAge(), stu.getSchool()))

if __name__ == '__main__':
    main()