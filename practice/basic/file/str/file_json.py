import json

userInfo = {'username': 'jack', 'age': 18}

BASE_FOLDER="D:\\timeFolder\\202111\\"
def json_fun():
    with open(BASE_FOLDER+'test.txt', 'w') as obj:
        json.dump(userInfo, obj)


    with open(BASE_FOLDER+'test.txt') as obj:
        content = json.load(obj)
        # content = obj.read()

        print(content)
    
    
def main():
    json_fun()
    
if __name__ == '__main__':
    main()

