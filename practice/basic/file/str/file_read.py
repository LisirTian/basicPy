import random

def file_read():
    try:
        file = open("D:\\timeFolder\\202107\\test.sql", "r")
        print("文件名: %s" % file.name)
        print("文件是否已关闭: %s" %file.closed)
        print(type(file))
        print("文件访问模式: %s" % file.mode)
    finally:
        file.close()
        print("关闭对象, 文件关闭状态: %s" % file.closed)

def file_read2():
    with open("D:\\timeFolder\\202107\\test.sql", "r", encoding="UTF-8") as file:
        val = file.readline()
        while val:
            print(val, end="")
            val=file.readline()

def file_read3():
    with open("D:\\timeFolder\\202107\\test.sql", "r", encoding="UTF-8") as file:
        for line in file:
            print(line, end="")



def main():
    file_read2()

if __name__ == '__main__':
    main()