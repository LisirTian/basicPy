from xml.etree import ElementTree as ET
import json
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = curPath[:curPath.find("basicPy\\")+len("basicPy\\")]

tree = ET.parse(rootPath+'res\\movie.xml')
root = tree.getroot()

all_data = []

for movie in root:
    # 存储电影数据的字典
    movie_data = {}
    # 存储属性的字典
    attr_data = {}

    # 取出 type 标签的值
    movie_type = movie.find('type')
    attr_data['type'] = movie_type.text

    # 取出 format 标签的值
    movie_format = movie.find('format')
    attr_data['format'] = movie_format.text

    # 取出 year 标签的值
    movie_year = movie.find('year')
    if movie_year:
        attr_data['year'] = movie_year.text

    # 取出 rating 标签的值
    movie_rating = movie.find('rating')
    attr_data['rating'] = movie_rating.text

    # 取出 stars 标签的值
    movie_stars = movie.find('stars')
    attr_data['stars'] = movie_stars.text

    # 取出 description 标签的值
    movie_description = movie.find('description')
    attr_data['description'] = movie_description.text

    # 获取电影名字，以电影名为字典的键，属性信息为字典的值
    movie_title = movie.attrib.get('title')
    movie_data[movie_title] = attr_data
    # 存入列表中
    all_data.append(movie_data)
if __name__ == '__main__':

    print(all_data)
    # all_data 此时是一个列表对象，用 json.dumps() 将python对象转换为 json 字符串
    json_str = json.dumps(all_data)
    print(json_str)

