'''

ElementTree 解析 xml 有三种方法：

调用parse()方法，返回解析树
tree = ET.parse('./resource/movie.xml')
root = tree.getroot()

调用from_string(),返回解析树的根元素
data = open('./resource/movie.xml').read()
root = ET.fromstring(data)

调用 ElementTree 类的 ElementTree(self, element=None, file=None) 方法
tree = ET.ElementTree(file="./resource/movie.xml")
root = tree.getroot()

'''
