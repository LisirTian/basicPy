import xlwings as xw

def xlsx2():
    app = xw.App(visible=True, add_book=False)
    wb = app.books.add()
    # wb = app.books.open("D:\\timeFolder\\202111\\test.xlsx")
    # print(app.books.active)

# https://docs.microsoft.com/zh-cn/office/vba/api/excel.chart(object) 微软chart文档
def xlsx3():
    app = xw.App(visible=True, add_book=False)
    wb = app.books.add()
    # wb = app.books.open("D:\\timeFolder\\202111\\test.xlsx")
    ws1 = wb.sheets[0]
    ws2=wb.sheets.add()
    # ws2 = wb.sheets[1]
    chart1 = ws1.charts.add(100, 100)  # 添加表格
    chart2 = ws1.charts.add(100, 320)
    chart1.chart_type = 'xy_scatter_lines_no_markers'  # 设置图标类型是xy散点连线图
    chart1.set_source_data(ws1.range(ws1.range((3, 1), (2396, 1)), ws1.range((3, 3), (2396, 3))))  # 选择任意两列
    tt12 = '这个是标题，您配吗？'  # 标题文本
    chart1.api[1].SetElement(2)  # 显示标题
    chart1.api[1].SetElement(101)  # 显示图例
    chart1.api[1].ChartTitle.Text = tt12  # 改变标题文本
    chart1.api[1].SetElement(301)  # x轴标题
    chart1.api[1].SetElement(311)  # 水平的y轴标题
    chart1.api[1].SetElement(305)  # y轴的网格线
    chart1.api[1].SetElement(334)  # x轴的网格线

    chart1.api[1].Axes(1).AxisTitle.Text = "wavelength(nm)"  # x轴标题的名字
    chart1.api[1].Axes(2).AxisTitle.Text = "power(dBm)"  # y轴标题的名字
    chart1.api[1].Axes(1).MinimumScale = 1500  # x 坐标轴最小值 1500nm
    chart1.api[1].Axes(1).MaximumScale = 1570  # x 坐标轴最大值 1570nm
    chart1.api[1].Axes(2).MinimumScale = 0  # y 坐标轴最小值
    chart1.api[1].Axes(2).MaximumScale = 1.3  # y坐标轴最大值
    chart1.api[1].Axes(1).MajorUnit = 10  # 坐标轴间隔


def xlsx1():
    wb = xw.Book("D:\\timeFolder\\202111\\数据表\\WMS字段.xlsx")
    print('sss')
    sheets = wb.sheets
    for sheet in sheets:
        print(sheet.name)

        for row in range(1,20):
            row_str = str(row)
            print(sheet.range('A' + row_str).value)
            # print(sheet.range('C' + row_str).value)
            # print(sheet.range('D' + row_str).value)

def main():
    xlsx3()

if __name__ == '__main__':
    main()