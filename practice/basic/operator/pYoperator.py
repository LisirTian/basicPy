# coding:UTF-8

# 地址相等
num_a = 2
num_b = 1+1
num_c = 4-2
'''
print("num_a变量地址: %d"  %id(num_a))
print("num_b变量地址: %d"  %id(num_b))
print("num_c变量地址: %d"  %id(num_c))
'''

# 身份运算符
num_d=10
num_e=10.0
print(num_d==num_e)
print("num_d和num_e比较: %s" % id(num_d), id(num_e),(num_d is num_e))
