# coding:UTF-8

dict1 = {"a": "aaa","b":"bbb",None:"ddd"}
print(dict1)

member = dict(name="aaa",age=18,score=97.5)
print(member)
if "name" in member:
    print("key is %s, value is: %s" % ("name", member["name"]))

for key,val in member.items():
    print("%s ==> %s" %(key,val))

member["aa"] = "aaa"
member.setdefault("K")
member.setdefault("name")
print(member)

member.update({"bb":"bbb"})
member.update({"bb":"bbbbb"})
print(member)

infos = dict.fromkeys(["aa","bb"], "ccc")
print(infos)

nums = dict(one=1, two = 2, thr =3)
print(sum(nums.values()))
nums["ddd"]="dg"
print(nums)

def main():
    print("")
if __name__ == '__main__':
    main()