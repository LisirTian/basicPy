
num = 10
# eval可以执行一行str语句,只能解析单行语句
result = eval("3*num")

print(result)

global_num = 10
global_str="数据加法计算: {}"
# 所有参数必需包装在字典当中, key就是eval()中使用的名称
var_dict = dict(num = global_num,info=global_str) # 字典
result = eval("info.format(num*3)",var_dict) # 传递字典
print(result)

print(var_dict)