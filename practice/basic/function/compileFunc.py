# coding:UTF-8
'''
mode = "eval" 只允许执行单行表达式
使用eval()函数可以直接计算表达式的结果并进行接收
mode="single"定义单行表达式,允许进行交互式处理
mode="eval"
'''
statment = "100+23-443"
code_eval=compile(statment,"","eval")
print(eval(code_eval))

input_data=None
statment="input_data= input('请输入你喜欢的学校: ')"
code_single = compile(statment,"", "single")
exec(code_single)
print("输入数据为: %s" % input_data)

infos=[]
statement3 = "for item in range(3):" \
             "    infos.append(input('请输入经常使用的网址: '))"
code_exec= compile(statement3,"","exec")
exec(code_exec)
exec("print('经常使用的网站是: %s' % infos)")