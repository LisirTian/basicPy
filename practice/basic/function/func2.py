def math(cmd, *numbers):
    """
    传入的参数是tuple 元组
    :param cmd:
    :param numbers:
    :return:
    """
    print(type(numbers))
    sum = 0
    if cmd=="+":
        for item in numbers:
            sum+=item
    return sum

print(math("+",1,2,3,4,5))

def print_info(name, **urls):
    """
    接收字典

    :param name:
    :param urls:
    :return:
    """
    print("name {}".format(name))
    for key, val in urls.items():
        print(key + " => " + val)

print_info("张三", baidu="www.baidu.com", google= "www.google.com")