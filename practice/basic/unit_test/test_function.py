import unittest
from name_function import get_formatted_name

class NameTestCase(unittest.TestCase):

    def test_name_function(self):
        full_name = get_formatted_name("david", "alex")
        self.assertEqual(full_name, "David Alex")

if __name__ == '__main__':
    unittest.main()