# coding:UTF-8
# 列表

infos = ["张三", "李四", "王五"]
print(infos[0], end="、")
print(infos[1], end="、")
print(infos[2], end="、")
print()
print(infos[-1], end="- ")
print(infos[-2], end="- ")
print(infos[-3], end="- ")
print()
info = ["张三", True, "王五"]
print(info[0], end=",")
print(info[1], end=",")
print(info[2])

for item in info:
    print(item)
print("===")
for index in range(len(infos)):
    print(infos[index])

for i in range(13):
    print(i, end=",")
print()
print(infos + info)

infos = ["张三", "李四", "王五","乔峰","欧阳修","慕容复","杨过"]
print(infos[-6:-3])
print(infos[3:])
print(infos[:3])
print("分片数据: %s" %infos[:3])

infos[2:3] = ["孙悟空", "猪八戒", "沙僧"]
print(infos)
# 从下标1开始,隔2个替换一个
infos[1:6:2] = ["剑一","剑2","剑3"]
print(infos)
