# coding:UTF-8
# 列表

infos = ["张三", "李四", "王五","乔峰","欧阳修","慕容复","杨过"]
print(infos[-6:-3])
print(infos[3:])
print(infos[:3])
print("分片数据: %s" %infos[:3])

infos[2:3] = ["孙悟空", "猪八戒", "沙僧"]
print(infos)
infos[1:6:2] = ["剑一","剑2","剑3"]
print(infos)

if "剑一" in infos :
    print("剑1数据存在!")
else:
    print("不存在")

infos.append("剑4")
print(infos)