def generator(): # 数据生成
    num_list=("basic-{num:0>20}".format(num=item) for item in range(999999999999999))
    return num_list

# yield 可以避免过大内存
def generator1(): # 数据生成
    print("yield前执行")
    yield "basic-001"
    print("yield后执行")
def generator2(maxNum): # 数据生成
    for item in range(1,maxNum):
        print("================start=================")
        yield "basic-{num:0>20}".format(num=item)
        print("================end=================")

def generator3(maxNum): # 数据生成
        print("================start=================")
        yield from range(1,maxNum)
        print("================end=================")


# def main():
#     result = generator()
#     for item in result:
#         print(item)
# def main1():
#     result = generator1()
#     print("main()调用next()函数获取yield返回内容: %s" % next(result))
# if __name__ == '__main__':
# def main():
#     for item in generator2(10):
#         print(item)
def main():
    res = generator3(10)
    for item in res:
        print(item)


if __name__ == '__main__':
    main()