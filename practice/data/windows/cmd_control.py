import pywinauto
from pywinauto.application import Application
import psutil

PID = 0
for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=['pid', 'name'])
        print(pinfo)
    except psutil.NoSuchProcess:
        pass

    if 'cmd.exe' == pinfo['name']:
        PID = pinfo['pid']
        break


app = Application(backend='uia').connect(process=PID)
# app = Application(backend="uia").start("cmd.exe")
# dialogWindow = pywinauto.application.Application().connect(process=PID,class_name="ConsoleWindowClass")
#
# window = dialogWindow.top_window()
window = app.top_window()
# window.type_keys("echo{SPACE} http://www.testertechnology.com/posts/add\r")
window.type_keys("echo{SPACE} http://www..com/posts/add")