import psutil

import pywinauto

from pywinauto.application import Application

'''
psutil 用于获取微信电脑版的进程信息；

pywinauto 用于自动化控制微信电脑版
'''


def main():
    global pinfo
    PID = 0
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name'])
            print(pinfo)
        except psutil.NoSuchProcess:
            pass

        if 'WeChat.exe' == pinfo['name']:
            PID = pinfo['pid']
            break


    app = Application(backend='uia').connect(process=PID)
    win = app['微信']
    pyq_btn = win.child_window(title="朋友圈", control_type="Button")
    cords = pyq_btn.rectangle()
    pywinauto.mouse.click(button='left', coords=(cords.left + 10, cords.top + 10))
    # print(pyq_win.dump_tree())


if __name__ == '__main__':
    main()
