# PySide2

from PySide2.QtWidgets import QApplication,QMessageBox

from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QFile

class Login:
    def __init__(self):
        # 从文件中加载UI定义
        qfile = QFile("login.ui")
        qfile.open(QFile.ReadOnly)
        qfile.close()
        # 从UI定义中动态 创建一个相应的窗口对象
        # 里面的控件对象成为窗口对象的属性了
        # 比如: self.ui.button, ...
        self.ui = QUiLoader().load(qfile)

        self.ui.pushButton_2.clicked.connect(self.handleCalc)

    def handleCalc(self):
        print("::::::::pushButton_2确定")

# import login.Ui_Form
if __name__=="__main__":
    import sys
    app=QApplication([])
    login =Login()
    login.ui.show()
    app.exec_()