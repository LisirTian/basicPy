
import sys,re

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMessageBox

from practice import (QLineEditEx)
from practice import (Ui_Form)
from practice import Script
from practice import NginxScript

# import login.Ui_Form

class InitForm(Ui_Form):
    # rootpath = os.getcwd().replace("\\", "/")
    rootpath = "D:/timeFolder/202111/hansProjectSetup"
    def __init__(self):
        self.qwidget = QtWidgets.QWidget()
        self.script = Script(self.rootpath)
        self.nginxScript = NginxScript(self.rootpath)
        self.dictConfig = self.script.initConfigFile()

    def setupUiEx(self,Form):
        self.lineEditJavaFilePath = QLineEditEx(Form)
        self.lineEditJavaFilePath.setEnabled(True)
        self.lineEditJavaFilePath.setGeometry(QtCore.QRect(100, 310, 171, 20))
        self.lineEditJavaFilePath.setText("")
        self.lineEditJavaFilePath.setObjectName("lineEditJavaFilePath")

    def configDictInfo(self):
        self.tableWidget.setColumnWidth(0, 400)
        self.tableWidget.setColumnWidth(1, 50)
        self.tableWidget.setColumnWidth(2, 50)

        self.lineEditMysqlPort.setText(self.dictConfig["mysql"]["port"])
        self.lineEditRedisPort.setText(self.dictConfig["redis"]["port"])
        self.lineEditMysqlBasedir.setText(self.dictConfig["mysql"]["basedir"])
        self.lineEditMysqlDatadir.setText(self.dictConfig["mysql"]["datadir"])
        self.lineEditRedisBasedir.setText(self.dictConfig["redis"]["basedir"])

        self.radioButtonMysqlInit.setChecked(self.dictConfig["mysql"]["isInit"])
        self.radioButtonMysqlAdd.setChecked(self.dictConfig["mysql"]["isAdd"])
        self.radioButtonMysqlStart.setChecked(self.dictConfig["mysql"]["isStart"])

        self.radioButtonMysqlInit.setEnabled(self.dictConfig["mysql"]["canInit"])
        self.radioButtonMysqlAdd.setEnabled(self.dictConfig["mysql"]["canAdd"])
        self.radioButtonMysqlStart.setEnabled(self.dictConfig["mysql"]["canStart"])
        self.pushButtonMysqlPortUpdate.setEnabled(self.dictConfig["mysql"]["canPortUpdate"])
        self.pushButtonMysqlPasswordSet.setEnabled(self.dictConfig["mysql"]["canPasswordUpdate"])

        self.radioButtonRedisAdd.setChecked(self.dictConfig["redis"]["isAdd"])
        self.radioButtonRedisStart.setChecked(self.dictConfig["redis"]["isStart"])
        self.radioButtonRedisStart.setEnabled(self.dictConfig["redis"]["canStart"])

        self.radioButtonNginxStart.setChecked(self.dictConfig["nginx"]["isStart"])
        self.pushButtonNginxReload.setEnabled(self.dictConfig["nginx"]["canReload"])

        arr_java = self.dictConfig["java"]

        for dictJava in arr_java:
            row = self.tableWidget.rowCount()
            self.tableWidget.insertRow(row)
            item = QtWidgets.QTableWidgetItem()
            item.setText(dictJava["jarPath"])
            item.setTextAlignment(0)

            self.tableWidget.setItem(row, 0, item)
            item = QtWidgets.QTableWidgetItem()
            item.setText(dictJava["port"])
            item.setToolTip("双击修改端口,默认为服务配置端口")
            self.tableWidget.setItem(row, 1, item)
            button = QtWidgets.QRadioButton("启动")
            button.setAutoExclusive(False)
            button.setChecked(dictJava["isStart"])
            button.clicked.connect(self.javaStart)
            self.tableWidget.setCellWidget(row, 2, button)

    def setupConfig(self):
        self.configDictInfo()

        self.pushButtonPortCheck.clicked.connect(self.portCheck)

        thread = MysqlInitThread(self.script)
        thread.sec_changed_signal.connect(self.mysqlInit)
        self.radioButtonMysqlInit.clicked.connect(lambda :thread.start())
        # self.radioButtonMysqlInit.clicked.connect(self.mysqlInit)
        self.radioButtonMysqlAdd.clicked.connect(self.mysqlAdd)
        self.radioButtonMysqlStart.clicked.connect(self.mysqlStart)

        self.pushButtonMysqlPortUpdate.clicked.connect(self.mysqlPortUpdate)
        self.pushButtonMysqlPasswordSet.clicked.connect(self.mysqlPasswordSet)

        self.radioButtonRedisAdd.clicked.connect(self.redisAdd)
        self.radioButtonRedisStart.clicked.connect(self.redisStart)

        self.radioButtonNginxStart.clicked.connect(self.nginxStart)
        self.pushButtonNginxReload.clicked.connect(self.nginxReload)

        self.tableWidget.itemChanged.connect(self.itemChanged)

    def mysqlInit(self):
        # self.script.mysqlInit()
        self.dictConfig["mysql"]["isInit"] = True
        self.dictConfig["mysql"]["canInit"] = False
        self.script.writeDictConfig(self.dictConfig)
        self.radioButtonMysqlInit.setEnabled(False)
        self.dictConfig["mysql"]["canPasswordUpdate"] = True
        self.pushButtonMysqlPasswordSet.setEnabled(True)
        self.dictConfig["mysql"]["canAdd"] = True
        self.dictConfig["mysql"]["password"] = "root"
        self.radioButtonMysqlAdd.setEnabled(True)
        self.script.writeDictConfig(self.dictConfig)
        self.script.mysqlPasswordSet("root")
        self.showInfo("Mysql初始化完成")

    def mysqlAdd(self):
        checked = self.radioButtonMysqlAdd.isChecked()
        if checked:
            add = self.script.mysqlServiceAdd()
            self.dictConfig["mysql"]["isAdd"] = True
            self.dictConfig["mysql"]["canStart"] = True
            self.radioButtonMysqlStart.setEnabled(True)
        else:
            add = self.script.mysqlServiceRemove()
            self.dictConfig["mysql"]["isAdd"] = False
            self.dictConfig["mysql"]["canStart"] = False
            self.radioButtonMysqlStart.setEnabled(False)
        self.script.writeDictConfig(self.dictConfig)
        self.showInfo(add)

    def nginxReload(self):
        self.nginxScript.nginxReload()
        self.showInfo("nginx重启完成")
    def nginxStart(self):
        checked = self.radioButtonNginxStart.isChecked()
        print(checked)
        if checked:
            self.nginxScript.nginxStart()
            start ="nginx启动完成"
            self.dictConfig["nginx"]["isStart"] = True
            self.dictConfig["nginx"]["canReload"] = True
            self.pushButtonNginxReload.setEnabled(True)
        else:
            self.nginxScript.nginxStop()
            start="nginx停止完成"
            self.dictConfig["nginx"]["isStart"] = False
            self.dictConfig["nginx"]["canReload"] = False
            self.pushButtonNginxReload.setEnabled(False)
        self.script.writeDictConfig(self.dictConfig)
        self.showInfo(start)


    def mysqlStart(self):
        checked = self.radioButtonMysqlStart.isChecked()
        print(checked)
        if checked:
            check_port = self.script.checkPort(self.dictConfig["mysql"]["port"])
            if check_port.find("未被占用") < 0:
                self.radioButtonMysqlStart.setChecked(False)
                self.showInfo("mysql端口被占用,请修改端口")
                return
            start = self.script.mysqlStart()
            self.dictConfig["mysql"]["isStart"]=True
            self.dictConfig["mysql"]["canPasswordUpdate"]=False
            self.dictConfig["mysql"]["canAdd"]=False
            self.dictConfig["mysql"]["canPortUpdate"]=False
            self.pushButtonMysqlPasswordSet.setEnabled(False)
            self.radioButtonMysqlAdd.setEnabled(False)
            self.pushButtonMysqlPortUpdate.setEnabled(False)
        else:
            start = self.script.mysqlStop()
            self.dictConfig["mysql"]["isStart"]=False
            self.dictConfig["mysql"]["canPasswordUpdate"] = True
            self.dictConfig["mysql"]["canAdd"] = True
            self.dictConfig["mysql"]["canPortUpdate"] = True
            self.pushButtonMysqlPasswordSet.setEnabled(True)
            self.radioButtonMysqlAdd.setEnabled(True)
            self.pushButtonMysqlPortUpdate.setEnabled(True)
        self.script.writeDictConfig(self.dictConfig)
        self.showInfo(start)
    def redisAdd(self):
        checked = self.radioButtonRedisAdd.isChecked()
        if checked:
            self.script.redisServiceAdd()
            self.radioButtonRedisAdd.setEnabled(False)
            self.dictConfig["redis"]["isAdd"] = checked
            self.dictConfig["redis"]["canStart"] = True
            self.radioButtonRedisStart.setEnabled(True)
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo("redis服务添加完成")
        else:
            self.script.redisServiceRemove()
            self.dictConfig["redis"]["isAdd"] = checked
            self.dictConfig["redis"]["canStart"] = False
            self.radioButtonRedisStart.setEnabled(False)
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo("redis服务移除完成")
    def redisStart(self):
        checked = self.radioButtonRedisStart.isChecked()
        print(checked)
        if checked:
            start = self.script.redisServiceStart()
            self.dictConfig["redis"]["isStart"] = True
            self.radioButtonRedisStart.setChecked(checked)
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo(start)
        else:
            stop = self.script.redisServiceStop()
            self.dictConfig["redis"]["isStart"] = False
            self.radioButtonRedisStart.setChecked(checked)
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo(stop)

    def portCheck(self):
        checkMysqlPort = self.script.checkPort(self.dictConfig["mysql"]["port"])
        checkRedisPort = self.script.checkPort(self.dictConfig["redis"]["port"])
        checkNginxPort = self.script.checkPort(self.dictConfig["nginx"]["port"])
        javaArr = self.dictConfig["java"]
        infoArr=[]
        infoArr.append(checkMysqlPort)
        infoArr.append(checkRedisPort)
        infoArr.append(checkNginxPort)
        for java in javaArr:
            port = java["port"]
            if len(port) > 0 and re.match("\d+$", port):
                check_port = self.script.checkPort(port)
                infoArr.append(check_port)
        self.textEditPortCheck.setText("\n".join(infoArr))
        # self.showInfo("\n".join(infoArr))

    def mysqlPortUpdate(self):
        port = self.lineEditMysqlPort.text()
        print(port)
        verify_port = self.verifyPort(port)
        if (not verify_port):
            return
        self.dictConfig["mysql"]["port"] = port
        self.script.writeDictConfig(self.dictConfig)
        self.script.writeMysqlIniPortConfig(port)
        self.showInfo("MySQL端口更改为" + port)

    def mysqlPasswordSet(self):
        text = self.lineEditMysqlPasswordSet.text()
        print(text)
        self.script.mysqlPasswordSet(text)
        self.dictConfig["mysql"]["password"] = text
        self.script.writeDictConfig(self.dictConfig)
        QMessageBox.information(QtWidgets.QWidget(), '温馨提示', "MySQL密码更改为" + text)

    def verifyPort(self, port):
        if(re.match("\d+$", port)):
            return True
        else:
            QMessageBox.information(QtWidgets.QWidget(), '温馨提示', "端口格式只能为数字,当前修改端口:" + port)
            return False
    def showInfo(self, msg):
        QMessageBox.information(self.qwidget,'温馨提示', msg)

    def itemChanged(self,item):
        row = self.tableWidget.row(item)
        print(row)
        column = self.tableWidget.column(item)
        print(column)
        port = item.text()
        if column==1:
            if len(port)>0:
                verify_port = self.verifyPort(port)
                if (not verify_port):
                    return
            self.dictConfig["java"][row]["port"] = port
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo("端口已修改为: " +port)

    # def cellClicked(self, row, column):
    #     print(row, column)
    #     print("cellClicked")
    #     for j in range(self.tableWidget.columnCount()):
    #         print(type(self.tableWidget.item(row, j)))
    #     print(type(self.tableWidget.cellWidget(row,2)))
    #     self.lineEditJavaFilePath.setText(self.tableWidget.item(row, 0).text())
    #     self.lineEditJavaPort.setText(self.tableWidget.item(row, 1).text())
    def javaStart(self):
        button = self.tableWidget.sender()
        print(type(button))
        row = self.tableWidget.currentRow()
        print(row, self.tableWidget.currentColumn())
        jarPath = self.tableWidget.item(row, 0).text()
        port = self.tableWidget.item(row, 1).text()
        print(jarPath, port)
        checked = button.isChecked()
        print(checked)
        if checked:
            if len(port) >0:
                check_port = self.script.checkPort(port=port)
                if check_port.find("未被占用") < 0:
                    self.radioButtonMysqlStart.setChecked(False)
                    self.showInfo("端口"+port+"被占用,请修改端口")
                    return
            self.script.javaStart(jarPath, port)
            self.dictConfig["java"][row]["isStart"]=True
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo(jarPath+" 服务启动完成")
        else:
            if len(port) <= 0:
                self.showInfo("请先设置服务端口号")
                return
            stop = self.script.javaStop(jarPath, port)
            self.dictConfig["java"][row]["isStart"] = False
            self.script.writeDictConfig(self.dictConfig)
            self.showInfo(port + " 端口解除占用\n"+ stop)





    def javaAdd(self):
        print("javaAdd")
        # row = self.tableWidget.rowCount()
        row = 0
        self.tableWidget.insertRow(row)
        item = QtWidgets.QTableWidgetItem()
        item.setText("sdsd")
        self.tableWidget.setItem(row, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setText("4433")
        item.setStatusTip("setStatusTip")
        item.setToolTip("setToolTip")
        item.setWhatsThis("setWhatsThis")
        self.tableWidget.setItem(row, 1, item)
        button = QtWidgets.QRadioButton("启动")
        button.setAutoExclusive(False)
        button.setChecked(True)
        button.clicked.connect(self.javaStart)
        self.tableWidget.setCellWidget(row, 2, button)

class MysqlInitThread(QtCore.QThread):
    sec_changed_signal = QtCore.pyqtSignal()  # 信号类型：int

    def __init__(self, script, parent=None):
        super().__init__(parent)
        self.script = script  # 默认1000秒

    def run(self):
        self.script.mysqlInit()
        self.sec_changed_signal.emit()


if __name__=="__main__":
    app=QtWidgets.QApplication(sys.argv)
    widget=QtWidgets.QWidget()
    ui=InitForm()
    ui.setupUi(widget)
    # ui.setupUiEx(widget)
    ui.setupConfig()


    widget.show()
    sys.exit(app.exec_())