# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'serviceConfig.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.setEnabled(True)
        Form.resize(629, 459)
        Form.setAutoFillBackground(False)
        self.horizontalLayoutWidget = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(30, 10, 581, 81))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.label_5 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.lineEditMysqlBasedir = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEditMysqlBasedir.setEnabled(True)
        self.lineEditMysqlBasedir.setObjectName("lineEditMysqlBasedir")
        self.verticalLayout_3.addWidget(self.lineEditMysqlBasedir)
        self.lineEditMysqlDatadir = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEditMysqlDatadir.setEnabled(True)
        self.lineEditMysqlDatadir.setText("")
        self.lineEditMysqlDatadir.setObjectName("lineEditMysqlDatadir")
        self.verticalLayout_3.addWidget(self.lineEditMysqlDatadir)
        self.lineEditRedisBasedir = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEditRedisBasedir.setEnabled(True)
        self.lineEditRedisBasedir.setText("")
        self.lineEditRedisBasedir.setObjectName("lineEditRedisBasedir")
        self.verticalLayout_3.addWidget(self.lineEditRedisBasedir)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(29, 100, 581, 73))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(48)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButtonPortCheck = QtWidgets.QPushButton(self.horizontalLayoutWidget_2)
        self.pushButtonPortCheck.setObjectName("pushButtonPortCheck")
        self.horizontalLayout_2.addWidget(self.pushButtonPortCheck)
        self.textEditPortCheck = QtWidgets.QTextEdit(self.horizontalLayoutWidget_2)
        self.textEditPortCheck.setEnabled(True)
        self.textEditPortCheck.setObjectName("textEditPortCheck")
        self.horizontalLayout_2.addWidget(self.textEditPortCheck)
        self.horizontalLayoutWidget_5 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_5.setGeometry(QtCore.QRect(30, 170, 381, 80))
        self.horizontalLayoutWidget_5.setObjectName("horizontalLayoutWidget_5")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_5)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setSpacing(12)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.labelMysql = QtWidgets.QLabel(self.horizontalLayoutWidget_5)
        self.labelMysql.setStyleSheet("")
        self.labelMysql.setObjectName("labelMysql")
        self.verticalLayout_4.addWidget(self.labelMysql)
        self.horizontalLayout_5.addLayout(self.verticalLayout_4)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.radioButtonMysqlInit = QtWidgets.QRadioButton(self.horizontalLayoutWidget_5)
        self.radioButtonMysqlInit.setAutoExclusive(False)
        self.radioButtonMysqlInit.setObjectName("radioButtonMysqlInit")
        self.verticalLayout_5.addWidget(self.radioButtonMysqlInit)
        self.radioButtonMysqlAdd = QtWidgets.QRadioButton(self.horizontalLayoutWidget_5)
        self.radioButtonMysqlAdd.setChecked(False)
        self.radioButtonMysqlAdd.setAutoExclusive(False)
        self.radioButtonMysqlAdd.setObjectName("radioButtonMysqlAdd")
        self.verticalLayout_5.addWidget(self.radioButtonMysqlAdd)
        self.radioButtonMysqlStart = QtWidgets.QRadioButton(self.horizontalLayoutWidget_5)
        self.radioButtonMysqlStart.setEnabled(True)
        self.radioButtonMysqlStart.setChecked(False)
        self.radioButtonMysqlStart.setAutoExclusive(False)
        self.radioButtonMysqlStart.setObjectName("radioButtonMysqlStart")
        self.verticalLayout_5.addWidget(self.radioButtonMysqlStart)
        self.horizontalLayout_5.addLayout(self.verticalLayout_5)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.horizontalLayoutWidget_5)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.lineEditMysqlPort = QtWidgets.QLineEdit(self.horizontalLayoutWidget_5)
        self.lineEditMysqlPort.setText("")
        self.lineEditMysqlPort.setObjectName("lineEditMysqlPort")
        self.horizontalLayout_3.addWidget(self.lineEditMysqlPort)
        self.pushButtonMysqlPortUpdate = QtWidgets.QPushButton(self.horizontalLayoutWidget_5)
        self.pushButtonMysqlPortUpdate.setObjectName("pushButtonMysqlPortUpdate")
        self.horizontalLayout_3.addWidget(self.pushButtonMysqlPortUpdate)
        self.verticalLayout_6.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_6 = QtWidgets.QLabel(self.horizontalLayoutWidget_5)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.lineEditMysqlPasswordSet = QtWidgets.QLineEdit(self.horizontalLayoutWidget_5)
        self.lineEditMysqlPasswordSet.setText("")
        self.lineEditMysqlPasswordSet.setObjectName("lineEditMysqlPasswordSet")
        self.horizontalLayout_4.addWidget(self.lineEditMysqlPasswordSet)
        self.pushButtonMysqlPasswordSet = QtWidgets.QPushButton(self.horizontalLayoutWidget_5)
        self.pushButtonMysqlPasswordSet.setObjectName("pushButtonMysqlPasswordSet")
        self.horizontalLayout_4.addWidget(self.pushButtonMysqlPasswordSet)
        self.verticalLayout_6.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5.addLayout(self.verticalLayout_6)
        self.horizontalLayoutWidget_6 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_6.setGeometry(QtCore.QRect(30, 260, 381, 31))
        self.horizontalLayoutWidget_6.setObjectName("horizontalLayoutWidget_6")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_6)
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.labelRedis = QtWidgets.QLabel(self.horizontalLayoutWidget_6)
        self.labelRedis.setObjectName("labelRedis")
        self.horizontalLayout_6.addWidget(self.labelRedis)
        self.radioButtonRedisAdd = QtWidgets.QRadioButton(self.horizontalLayoutWidget_6)
        self.radioButtonRedisAdd.setEnabled(True)
        self.radioButtonRedisAdd.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.radioButtonRedisAdd.setAutoFillBackground(False)
        self.radioButtonRedisAdd.setAutoExclusive(False)
        self.radioButtonRedisAdd.setObjectName("radioButtonRedisAdd")
        self.horizontalLayout_6.addWidget(self.radioButtonRedisAdd)
        self.radioButtonRedisStart = QtWidgets.QRadioButton(self.horizontalLayoutWidget_6)
        self.radioButtonRedisStart.setEnabled(True)
        self.radioButtonRedisStart.setChecked(False)
        self.radioButtonRedisStart.setAutoExclusive(False)
        self.radioButtonRedisStart.setObjectName("radioButtonRedisStart")
        self.horizontalLayout_6.addWidget(self.radioButtonRedisStart)
        self.label_4 = QtWidgets.QLabel(self.horizontalLayoutWidget_6)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_6.addWidget(self.label_4)
        self.lineEditRedisPort = QtWidgets.QLineEdit(self.horizontalLayoutWidget_6)
        self.lineEditRedisPort.setEnabled(False)
        self.lineEditRedisPort.setText("")
        self.lineEditRedisPort.setObjectName("lineEditRedisPort")
        self.horizontalLayout_6.addWidget(self.lineEditRedisPort)
        self.horizontalLayoutWidget_7 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_7.setGeometry(QtCore.QRect(30, 300, 381, 31))
        self.horizontalLayoutWidget_7.setObjectName("horizontalLayoutWidget_7")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_7)
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.labelRedis_2 = QtWidgets.QLabel(self.horizontalLayoutWidget_7)
        self.labelRedis_2.setObjectName("labelRedis_2")
        self.horizontalLayout_7.addWidget(self.labelRedis_2)
        self.radioButtonNginxStart = QtWidgets.QRadioButton(self.horizontalLayoutWidget_7)
        self.radioButtonNginxStart.setEnabled(True)
        self.radioButtonNginxStart.setChecked(False)
        self.radioButtonNginxStart.setAutoExclusive(False)
        self.radioButtonNginxStart.setObjectName("radioButtonNginxStart")
        self.horizontalLayout_7.addWidget(self.radioButtonNginxStart)
        self.pushButtonNginxReload = QtWidgets.QPushButton(self.horizontalLayoutWidget_7)
        self.pushButtonNginxReload.setObjectName("pushButtonNginxReload")
        self.horizontalLayout_7.addWidget(self.pushButtonNginxReload)
        self.label_7 = QtWidgets.QLabel(self.horizontalLayoutWidget_7)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_7.addWidget(self.label_7)
        self.lineEditRedisPort_2 = QtWidgets.QLineEdit(self.horizontalLayoutWidget_7)
        self.lineEditRedisPort_2.setEnabled(False)
        self.lineEditRedisPort_2.setText("")
        self.lineEditRedisPort_2.setObjectName("lineEditRedisPort_2")
        self.horizontalLayout_7.addWidget(self.lineEditRedisPort_2)
        self.horizontalLayoutWidget_8 = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget_8.setGeometry(QtCore.QRect(30, 340, 581, 111))
        self.horizontalLayoutWidget_8.setObjectName("horizontalLayoutWidget_8")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_8)
        self.horizontalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.labelRedis_3 = QtWidgets.QLabel(self.horizontalLayoutWidget_8)
        self.labelRedis_3.setObjectName("labelRedis_3")
        self.horizontalLayout_8.addWidget(self.labelRedis_3)
        self.tableWidget = QtWidgets.QTableWidget(self.horizontalLayoutWidget_8)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.horizontalLayout_8.addWidget(self.tableWidget)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Setup"))
        self.label.setText(_translate("Form", "Mysql路径"))
        self.label_2.setText(_translate("Form", "Mysql数据文件夹路径"))
        self.label_5.setText(_translate("Form", "Redis路径"))
        self.pushButtonPortCheck.setText(_translate("Form", "端口检查"))
        self.labelMysql.setText(_translate("Form", "MySQL        "))
        self.radioButtonMysqlInit.setText(_translate("Form", "初始化MySQL"))
        self.radioButtonMysqlAdd.setText(_translate("Form", "服务添加/移除"))
        self.radioButtonMysqlStart.setText(_translate("Form", "服务启动/停止"))
        self.label_3.setText(_translate("Form", "端口"))
        self.lineEditMysqlPort.setPlaceholderText(_translate("Form", "3306"))
        self.pushButtonMysqlPortUpdate.setText(_translate("Form", "修改端口"))
        self.label_6.setText(_translate("Form", "密码"))
        self.lineEditMysqlPasswordSet.setPlaceholderText(_translate("Form", "默认root"))
        self.pushButtonMysqlPasswordSet.setText(_translate("Form", "修改密码"))
        self.labelRedis.setText(_translate("Form", "Redis         "))
        self.radioButtonRedisAdd.setText(_translate("Form", "服务添加/移除"))
        self.radioButtonRedisStart.setText(_translate("Form", "服务启动/停止"))
        self.label_4.setText(_translate("Form", "端口"))
        self.lineEditRedisPort.setPlaceholderText(_translate("Form", "6379"))
        self.labelRedis_2.setText(_translate("Form", "Nginx         "))
        self.radioButtonNginxStart.setText(_translate("Form", "服务启动/停止"))
        self.pushButtonNginxReload.setText(_translate("Form", "重启服务"))
        self.label_7.setText(_translate("Form", "    端口"))
        self.lineEditRedisPort_2.setPlaceholderText(_translate("Form", "80"))
        self.labelRedis_3.setText(_translate("Form", "Java    "))
        self.tableWidget.setSortingEnabled(False)
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "文件"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Form", "端口"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("Form", "状态"))
