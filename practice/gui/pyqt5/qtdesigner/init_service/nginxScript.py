import subprocess, os


class NginxScript():
    def __init__(self, rootpath):
        self.rootpath = rootpath
        self.nginxBasedir = self.rootpath +"/nginxSetup/nginx"
        self.startupPath = "C:/ProgramData/Microsoft/Windows/Start Menu/Programs/StartUp"

    def nginxStart(self):
        cmd = "cd ./nginxSetup/nginx & start nginx"
        print(cmd)
        # system = os.system(cmd)
        popen = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        with open(self.startupPath+ "/nginx.bat", "w", encoding="UTF-8") as file:
            str='''D:
cd D:/timeFolder/202111/hansProjectSetup/nginxSetup/nginx
start nginx
            '''
            file.write(str)
        # line = popen.stdout.readlines()
        # print(line)

    def nginxStop(self):
        cmd = "cd ./nginxSetup/nginx & nginx -s stop"
        # system = os.system(cmd)
        popen = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        if os.path.exists(self.startupPath+ "/nginx.bat"):
            os.remove(self.startupPath+ "/nginx.bat")
        # line = popen.stdout.readlines()
        # print(line)
    def nginxReload(self):
        cmd = "cd ./nginxSetup/nginx & nginx -s reload"
        # system = os.system(cmd)
        popen = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        line = popen.stdout.readlines()
        print(line)