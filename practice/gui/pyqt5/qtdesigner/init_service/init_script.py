import os, time, subprocess, re, json

class Script():
    # rootpath = os.getcwd().replace("\\", "/")
    # rootpath = "D:/timeFolder/202111/hansProjectSetup"
    def __init__(self, rootpath):
        self.rootpath = rootpath
        self.mysql_service_name = "mysqlHans"
        self.mysqlBasedir = self.rootpath +"/mysqlSetup/mysql-8.0.27-winx64"
        self.mysqld = self.mysqlBasedir +"/bin/mysqld.exe"
        self.myini_path=self.mysqlBasedir +"/my.ini"
        self.startupPath = "C:/ProgramData/Microsoft/Windows/Start Menu/Programs/StartUp"

    def initConfigFile(self):
        self.configfilePath = self.rootpath+"/config.json"
        dictConfig={}
        if not os.path.exists(self.configfilePath):
            dictConfig = self.getInitConfig()
            self.writeDictConfig(dictConfig)
            self.mysqlIniConfigInit()
        else:
            dictConfig=self.readDictConfig()
        return dictConfig
    def writeDictConfig(self, dictConfig):
        with open(self.configfilePath, "w", encoding="UTF-8") as file:
            json.dump(dictConfig, file, indent=4)
    def readDictConfig(self):
        with open(self.configfilePath) as obj:
            dictConfig = json.load(obj)
        return dictConfig
    def mysqlInit(self):
        cmd = self.mysqld + " --initialize --console"
        # system = os.system(cmd)
        popen = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        line = popen.stdout.readlines()
        print(line)
    def mysqlServiceAdd(self):
        cmdAddService=self.mysqld+ " --install "+self.mysql_service_name \
                      +" --defaults-file=" +self.myini_path
        # print(cmdAddService)
        # system = os.system(cmdAddService)
        # print(system)
        line = self.getCmdResult(cmdAddService)
        return line
    def mysqlServiceRemove(self):
        # system = os.system(self.mysqld + " --remove " + self.mysql_service_name)
        line = self.getCmdResult(self.mysqld + " --remove " + self.mysql_service_name)
        return line
    def getCmdResult(self, cmd):
        popen = subprocess.Popen(cmd,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        line = popen.stdout.readline()
        return line
    def mysqlStart(self):
        popen = subprocess.Popen("net start " + self.mysql_service_name,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')

        lines = popen.stdout.readlines()
        return "".join(lines).strip().split("\n")[-1]
    def mysqlStop(self):
        popen = subprocess.Popen("net stop " + self.mysql_service_name,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        lines = popen.stdout.readlines()
        return "".join(lines).strip().split("\n")[-1]

    def mysqlPasswordSet(self, password):
        with open(self.rootpath +"/mysqlSetup/sqlTmp.sql", "a", encoding="UTF-8") as file:
            file.write("ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '"+password+"';")
        temp = self.mysqld + " --init-file="+self.rootpath +"/mysqlSetup/sqlTmp.sql"
        print(temp)
        p=subprocess.Popen(temp,
                         stdout=subprocess.PIPE,
                         # creationflags=subprocess.CREATE_NO_WINDOW,
                         shell=True,
                         stderr=subprocess.STDOUT,
                         stdin=subprocess.PIPE,
                         encoding='GBK')
        p.stdin.close()
        # system = os.system(temp)
        # time.sleep(1)
        shutdown = self.mysqlBasedir + "/bin/mysqladmin.exe shutdown -uroot -p" + password
        print(shutdown)
        popen=subprocess.Popen(shutdown,
                         stdout=subprocess.PIPE,
                         creationflags=subprocess.CREATE_NO_WINDOW,
                         shell=True,
                         stderr=subprocess.STDOUT,
                         stdin=subprocess.PIPE,
                         encoding='GBK')
        popen.stdin.close()
        lines = popen.stdout.readlines()
        print(lines)
        # os.system(shutdown)
        os.remove(self.rootpath +"/mysqlSetup/sqlTmp.sql")

    def redisServiceAdd(self):
        console = self.rootpath+"/redisSetup/Redis/redis-server.exe --service-install "+self.rootpath+"/redisSetup/Redis/redis.windows.conf --loglevel verbose"
        print(console)
        popen = subprocess.Popen(console,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        # os.system(console)
        # line = popen.stdout.readlines()
        # print(line)
    def redisServiceRemove(self):
        console=self.rootpath+"/redisSetup/Redis/redis-server.exe --service-uninstall"
        # console="sc delete redis"
        print(console)
        popen = subprocess.Popen(console,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        lines = popen.stdout.readlines()
        print(lines)
    def redisServiceStart(self):
        console = self.rootpath + "/redisSetup/Redis/redis-server.exe --service-start"
        print(console)
        popen = subprocess.Popen(console,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        line = popen.stdout.readline()
        print(line)
        return line
    def redisServiceStop(self):
        console = self.rootpath + "/redisSetup/Redis/redis-server.exe --service-stop"
        print(console)
        popen = subprocess.Popen(console,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        line = popen.stdout.readline()
        print(line)
        return line

    def checkPort(self, port):
        pid=None
        popen = subprocess.Popen("netstat -ano",
                                stdout = subprocess.PIPE,
                                creationflags = subprocess.CREATE_NO_WINDOW,
                                shell = True,
                                stderr = subprocess.STDOUT,
                                stdin = subprocess.PIPE,
                                encoding = 'GBK')
        popen.stdin.close()
        while popen.poll() is None:
            line = popen.stdout.readline()
            line = line.strip()
            items = line.split()
            # print(items)
            length = len(items)
            for item in items:
                if (re.match("\\d+\\.\\d+\\.\\d+\\.\\d+:"+port+"$", item)):
                    if item ==items[1]:
                        pid = items[length - 1]
                        # print(pid)
                        break
            # print(line)
# "tasklist /fi "pid eq 42808" /nh"
        if (pid is not None):
            p = subprocess.Popen("tasklist /fi \"pid eq "+pid+"\" /nh",
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
            popen.stdin.close()
            while p.poll() is None:
                line = p.stdout.readline()
                line = line.strip()
                if len(line)>0:
                    # print(line)
                    items = line.split()
                    taskName = items[0].lower()
                    # print(taskName)
                    if taskName.find("mysql") != -1:
                        taskName = "MySQL"
                    elif taskName.find("redis") != -1:
                        taskName = "Redis"
                    elif taskName.find("java") != -1:
                        taskName = "Java"
                    elif taskName.find("nginx") != -1:
                        taskName = "Nginx"
                    else:
                        taskName=""
                    # print(port)
                    # print(taskName)
                    # print(pid)
                    return "端口 " + port + " 已被 " + taskName + "(PID: " + pid + ") 占用."
        return "端口 " + port + " 未被占用."
    def getInitConfig(self):
        pwd = self.rootpath
        dictMysql = {}
        dictMysql["port"]="3306"
        dictMysql["basedir"]=pwd + "/mysqlSetup/mysql-8.0.27-winx64"
        dictMysql["datadir"] = pwd + "/mysqlSetup/mysql-8.0.27-winx64/data"
        dictMysql["serviceName"] ="mysqlHans"
        dictMysql["isInit"]=False
        dictMysql["canInit"]=True
        dictMysql["isAdd"]=False
        dictMysql["canAdd"]=False
        dictMysql["isStart"]=False
        dictMysql["canStart"]=False
        dictMysql["canPortUpdate"]=True
        dictMysql["canPasswordUpdate"]=False
        dictMysql["password"]="root"

        dictRedis ={}
        dictRedis["serviceName"]="Redis"
        dictRedis["basedir"]=pwd+"/redisSetup/Redis"
        dictRedis["port"]="6379"
        dictRedis["isAdd"] = False
        dictRedis["isStart"] = False
        dictRedis["canStart"] = False
        dictNginx={}
        dictNginx["isStart"]=False
        dictNginx["canReload"]=False
        dictNginx["port"]="80"
        arrJava=[]
        listdir = os.listdir(self.rootpath + "/javaSetup")
        for item in listdir:
            jarFile = self.rootpath + "/javaSetup/" + item
            if os.path.isfile(jarFile) and jarFile.endswith(".jar"):
                dictJava={}
                dictJava["jarPath"]=jarFile
                dictJava["port"]=""
                dictJava["isStart"]=False
                arrJava.append(dictJava)

        dictConfig={"mysql": dictMysql, "redis": dictRedis, "nginx":dictNginx, "java":arrJava}
        return dictConfig

    def writeMysqlIniPortConfig(self, port):
        list = []
        with open(self.myini_path, "r", encoding="UTF-8") as file:
            for line in file.readlines():  # 依次读取每行
                line = line.strip()  # 去掉每行头尾空白
                if line.startswith("port="):
                    line = "port=" + port
                list.append(line)
        with open(self.myini_path, "w", encoding="UTF-8") as file:
            file.write("\n".join(list))
    def mysqlIniConfigInit(self):
        list=[]
        with open(self.myini_path, "r", encoding="UTF-8") as file:
            for line in file.readlines():  # 依次读取每行
                line = line.strip()  # 去掉每行头尾空白
                if line.startswith("basedir="):
                    line= "basedir="+self.mysqlBasedir
                elif line.startswith("datadir="):
                    line= "datadir="+self.mysqlBasedir+ "/data"
                elif line.startswith("port="):
                    line = "port=3306"
                list.append(line)
        with open(self.myini_path, "w", encoding="UTF-8") as file:
            file.write("\n".join(list))

    def javaStart(self, jarPath, port):
        console = "START " + self.rootpath + "/javaSetup/jdk14/bin/javaw.exe -jar "+jarPath + " --spring.datasource.password="+self.readDictConfig()["mysql"]["password"]
        if len(port) >0:
            console = console+ " --server.port="+ port
        print(console)
        popen = subprocess.Popen(console,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        rfind = jarPath[jarPath.rfind("/"): jarPath.rfind(".")]
        with open(self.startupPath + rfind+".bat", "w", encoding="UTF-8") as file:
            str = console
            file.write(str)
        # line = popen.stdout.readline()
        # print(line)
        # return line

    def javaStop(self, jarPath, port):
        pid = None
        popen = subprocess.Popen("netstat -ano",
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
        popen.stdin.close()
        rfind = jarPath[jarPath.rfind("/"): jarPath.rfind(".")]
        if os.path.exists(self.startupPath + rfind+".bat"):
            os.remove(self.startupPath + rfind+".bat")
        while popen.poll() is None:
            line = popen.stdout.readline()
            line = line.strip()
            items = line.split()
            # print(items)
            length = len(items)
            for item in items:
                if (re.match("\\d+\\.\\d+\\.\\d+\\.\\d+:" + port, item)):
                    pid = items[length - 1]
                    break
        if (pid is not None):
            p = subprocess.Popen("taskkill /f /pid " + pid,
                                 stdout=subprocess.PIPE,
                                 creationflags=subprocess.CREATE_NO_WINDOW,
                                 shell=True,
                                 stderr=subprocess.STDOUT,
                                 stdin=subprocess.PIPE,
                                 encoding='GBK')
            p.stdin.close()
            line = p.stdout.readline()
            print(line)
            return line
        else:
            return "端口未被占用"


def main():
    script= Script()
    while (True):
        menu= """
Redis  Port: 6379
MySQL  Port: 3306
-----------------------------------------------------------------
1  -  启动Redis                          2  -  停止Redis
0  -  初始化MySQL
3  -  添加MySQL服务                       4  -  移除MySQL服务
5  -  启动MySQL                          6  -  停止MySQL
                                         8  -  设置MySQL端口
9  -  检查端口状态                         10  -  设置MySQL root密码
-----------------------------------------------------------------
        """
        # i = os.system("cls")
        print(menu)
        inputMsg = input("请选择:")
        if (inputMsg=="0"):
            script.mysqlInit()
        elif (inputMsg=="3"):
            add = script.mysqlServiceAdd()
        elif (inputMsg=="4"):
            script.mysqlServiceRemove()
        elif (inputMsg=="5"):
            script.mysqlStart()
        elif (inputMsg=="6"):
            script.mysqlStop()
        elif (inputMsg=="9"):
            info = script.checkPort("3307")
            print(info)
        elif (inputMsg=="10"):
            script.mysqlPasswordSet("root")
        else:
            pass


if __name__ == '__main__':
    main()