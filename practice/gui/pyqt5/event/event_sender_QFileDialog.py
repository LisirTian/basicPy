import sys
from PyQt5.QtWidgets import (QMainWindow, QTextEdit,
                             QAction, QFileDialog, QApplication)
from PyQt5.QtGui import QIcon

import matplotlib.pyplot as plt#约定俗成的写法plt
import re
import numpy as np

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.statusBar()

        openFile = QAction(QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showDialog)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(openFile)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('File dialog')
        self.show()

    def showDialog(self):
        fname = QFileDialog.getOpenFileName(self, '打开文件', '/resource/ZHCLF_12/s_parameter/X0-Y0','*.s2p;;*.s5p') # *.s2p;;*.s5p
        # 两个参数时 选择文件和文件类型和分在两个str里,
        # 一个参数时,两个数据放在一个元组里
        # fname,ss = QFileDialog.getOpenFileName(self, '打开文件', '/resource','Snp Files(*.s2p *.s5p)')
        print(fname)
        # print(ss)
        if fname[0]:
            f = open(fname[0], 'r')

            with f:
                list = f.readlines()
                # data = f.read()
                self.textEdit.setText("".join(list))

                # 二维数据集
                twoDList = []
                for str in list:
                    if (str.startswith("!") or str.startswith("#")):
                        pass
                    elif (re.match("^\\d.*", str)): # 数字.*
                        oneRowList = str.split()
                        twoDList.append(oneRowList)
                twoDListGoal = ij2jiConvert(twoDList)
                # for aa in twoDListGoal:
                #     print(aa)
                rows = len(twoDListGoal)
                for i in range(rows):
                    if i==0:
                        x_data = twoDListGoal[i]
                    else:
                        if i%2==0:
                            continue
                        y_data = np.array(twoDListGoal[i]).astype(np.float16)
                        print(y_data)
                        plt.plot(x_data, y_data)
                # x_data = ['2011', '2012', '2013', '2014', '2015', '2016', '2017']
                # y_data = [58000, 60200, 63000, 71000, 84000, 90500, 107000]

                plt.show()

def ij2jiConvert(twoDList):
    rows = len(twoDList)
    columns = len(twoDList[0])
    twoDListGoal = [[0] * rows for i in range(columns)]
    for i in range(rows):
         for j in range(columns):
            twoDListGoal[j][i]=twoDList[i][j]
    return twoDListGoal





if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())