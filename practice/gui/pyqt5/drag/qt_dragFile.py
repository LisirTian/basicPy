from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import sys


class LineEditEx(QLineEdit):
    def __init__(self, parent=None):
        super().__init__(None, parent)
        self.setGeometry(50, 50, 100, 20)
        self.setAcceptDrops(True)
        self.setDragEnabled(True)  # 开启可拖放事件

    def dragEnterEvent(self, QDragEnterEvent):
        e = QDragEnterEvent  # type:QDragEnterEvent
        print('type:', e.type())  # 事件的类型
        print('pos:', e.pos())  # 拖放位置
        print(e.mimeData().urls())  # 文件所有的路径
        print(e.mimeData().text().split('\n')[0].split('///')[1],type(e.mimeData().text()))  # 文件路径
        print(e.mimeData())
        print(e.mimeData().formats())  # 支持的所有格式
        print(e.mimeData().data('text/plain'))  # 根据mime类型取路径 值为字节数组
        print(e.mimeData().hasText())  # 是否支持文本文件格式
        if e.mimeData().hasText():
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        self.setText(e.mimeData().text()) #如果之前设置ignore 为False 这里将不会生效

class Example(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setUi()

    def setUi(self):
        self.setGeometry(100, 100, 300, 300)
        self.setWindowTitle('demo_ui_2')
        self.textEdit = LineEditEx(self)

        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())