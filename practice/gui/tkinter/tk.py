import tkinter
import os
import sys

def get_resource_path(relative_path):
    if getattr(sys, "frozen",False):
        base_path=sys._MEIPASS
    else:
        base_path=os.path.abspath("")
    return os.path.join(base_path, relative_path)

# print(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOGO_PATH = get_resource_path(os.path.join("res","favicon.ico"))
    # "res"+os.sep+"favicon.ico"
class MainForm: # 定义窗体类
    def __init__(self): # 构造方法里面进行窗体的控制
        root= tkinter.Tk() # 创建一个窗体
        root.title("pyGUI")
        root.iconbitmap(LOGO_PATH)
        root.geometry("500x100") #初始化尺寸
        root.maxsize(1000,400) #最大尺寸
        root["background"]="LightSlateGray"
        # -------------文本标签定义----------------
        label_text= tkinter.Label(root, text="basic", width=200,height=200,bg="#121122",fg="#aaccbb",
                                  justify="right")
        # label_text.pack() #组件的布局表示组件要进行显示





        root.mainloop() # 循环显示窗体

def main():
    MainForm() # 实例化窗体类
if __name__ == '__main__':
    main()