import numpy as np

import matplotlib.pyplot as plt


def plot_circle(center=(3, 3), r=2):
    """

    :param center: 圆心
    :param r: 半径
    :return:
    """
    x = np.linspace(center[0] - r, center[0] + r, 5000)

    y1 = np.sqrt(r ** 2 - (x - center[0]) ** 2) + center[1]
    y2 = -np.sqrt(r ** 2 - (x - center[0]) ** 2) + center[1]

    plt.plot(x, y1, c='k')
    plt.plot(x, y2, c='k')
    plt.axis("equal")
    plt.show()


def plot_circle2(center=(3, 3), r=2):
    theta = np.arange(0, 2 * np.pi, 0.01)
    x = center[0] + r * np.cos(theta)
    y = center[1] + r * np.sin(theta)

    plt.plot(x, y, c='r')
    plt.axis("equal")
    plt.show()


if __name__ == '__main__':
    plot_circle2(r=3)
