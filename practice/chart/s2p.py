from pyecharts.charts import Line
from pyecharts import options as opts


def getData(fileName):
    baseDir = "E:\www\matlab\data"

    file = baseDir + "\\" + fileName
    dataArr=[[],[],[],[],[],[],[],[],[]]
    xArr= []
    yArr1= []
    with open(file, "r", encoding="UTF-8") as file:
        for line in file:
            if (not (line.startswith("!") or line.startswith("#"))):
                # print(line, end="")
                rows = line.split()
                # print(rows)
                # xArr.append(float(rows[0]))
                # yArr1.append(float(rows[1]))
                for i in range(len(rows)):
                    # print(i)
                    dataArr[i].append(float(rows[i]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # dataArr[0].append(float(rows[0]))
                # print(len(xArr))
    return dataArr

def getAllData():
    file1 = "14.S2P"
    file2 = "5.6.S2P"
    file3 = "11.2.S2P"
    file4 = "18.S2P"
    dataArr = getData(file1)
    xArr = dataArr[0]
    yArr = dataArr[2]
    yArr2 = dataArr[5]
    yArr3 = dataArr[3]
    yArr4=[]
    for i in range(len(yArr2)):
        yArr4.append(yArr2[i]-yArr3[i])
    # print(yArr4)
    yAllArr=[yArr,yArr2,yArr3,yArr4]
    return xArr,yAllArr

def draw():
    xArr,yAllArr = getAllData()
    line = Line()
    line.set_global_opts(xaxis_opts=opts.AxisOpts(type_="value"))

    line.add_xaxis(xArr)
    for i in range(len(yAllArr)):
        line.add_yaxis("chart"+str(i), yAllArr[i], is_symbol_show=False)

    # line.add_xaxis([2, 3, 4, 6])
    # line.add_yaxis("test", [44, 55, 66, 89],is_symbol_show= False)

    # line.height="500px"
    line.width = "100%"
    line.render("s2p.html")


if __name__ == '__main__':
    draw()
