import wx
import cv2
import colordetect as cd
# import colordraw
class CubeFrame(wx.Frame):

    def __init__(self,*args,**kwargs):
        super(CubeFrame,self).__init__(*args,**kwargs)

        self.SetClientSize(600,680)
        self.SetMaxSize((600,680))
        self.SetMinSize((600,680))
        self.Bind(wx.EVT_IDLE,self.onIDLE)
        self.panel = wx.Panel(self, wx.ID_ANY)

        labelOne = wx.StaticText(self.panel, wx.ID_ANY, '机器人串口:')
        inputTxtOne = wx.TextCtrl(self.panel, wx.ID_ANY, 'COM3')
        labelTwo = wx.StaticText(self.panel, wx.ID_ANY, '摄像头串口:')
        inputTxtTwo = wx.TextCtrl(self.panel, wx.ID_ANY, '0')
        self.Bind(wx.EVT_TEXT,self.onCOM1,inputTxtOne)
        self.Bind(wx.EVT_TEXT, self.onCOM2, inputTxtTwo)

        self.cap=None
        self.index = 0
        self.COM2=0
        self.COM1="COM3"
        self.names=["up.jpg","down.jpg","left.jpg","right.jpg","front.jpg","back.jpg"]
        labelThree = wx.StaticText(self.panel, wx.ID_ANY, '魔方:')
        choice=wx.Choice(self.panel,choices=["上面","下面","左面","右面","前面","后面"])
        choice.SetSelection(self.index)
        self.Bind(wx.EVT_CHOICE,self.onChoice,choice)

        self.cameraBtn = wx.Button(self.panel, wx.ID_ANY, '拍照保存')
        self.cubeBtn = wx.Button(self.panel, wx.ID_ANY, '魔方还原')
        openBtn = wx.Button(self.panel, wx.ID_ANY, '打开摄像头')
        self.cameraBtn.Disable()
        #self.cubeBtn.Disable()
        self.Bind(wx.EVT_BUTTON, self.onCamera, self.cameraBtn)
        self.Bind(wx.EVT_BUTTON, self.onCube, self.cubeBtn)
        self.Bind(wx.EVT_BUTTON,self.onOpen,openBtn)

        self.pic=wx.StaticBitmap(self.panel)
        self.pic.SetSize((600,500))
        img=wx.Bitmap()
        img.Create(600,500)
        self.pic.SetBitmap(img)


        topSizer = wx.BoxSizer(wx.VERTICAL)
        inputOneSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)

        inputOneSizer.Add(labelOne, 0, wx.ALL, 5)
        inputOneSizer.Add(inputTxtOne, 1, wx.ALL | wx.EXPAND, 5)
        inputOneSizer.Add(labelTwo, 0, wx.ALL, 5)
        inputOneSizer.Add(inputTxtTwo, 1, wx.ALL | wx.EXPAND, 5)
        inputOneSizer.Add(labelThree, 0, wx.ALL | wx.EXPAND, 5)
        inputOneSizer.Add(choice, 1, wx.ALL | wx.EXPAND, 5)
        btnSizer.Add(openBtn, 0, wx.ALL, 5)
        btnSizer.Add(self.cameraBtn, 0, wx.ALL, 5)
        btnSizer.Add(self.cubeBtn, 0, wx.ALL, 5)
        topSizer.Add(inputOneSizer, 0, wx.ALL | wx.EXPAND, 5)
        topSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL | wx.EXPAND, 5)
        topSizer.Add(self.pic, 0, wx.ALL | wx.EXPAND, 5)
        topSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL | wx.EXPAND, 5)
        topSizer.Add(btnSizer, 0, wx.ALL | wx.CENTER, 5)
        self.panel.SetSizer(topSizer)
        #topSizer.Fit(self)
        self.Show(True)

    #打开摄像头按钮点击事件
    def onOpen(self,event):
        try:
            self.cap = cv2.VideoCapture(self.COM2)
            if self.cap.isOpened()==False:
                self.cap.release()
                self.cap.retrieve()
                self.cap==None
                wx.MessageDialog(self, "打开摄像头失败,检查串口是否正确").ShowModal()
            else:
                self.cameraBtn.Enable()
        except Exception as e:
            wx.MessageDialog(self,"打开摄像头失败,检查串口是否正确").ShowModal()
            self.cap = None
            print("打开摄像头失败")
            print(str(e))


    def onCOM1(self,event):
        self.COM1=event.GetEventObject().GetLineText(0)
        #print("COM1:"+self.COM1)

    def onCOM2(self,event):
        self.COM2=event.GetEventObject().GetLineText(0)
        #print("COM2:" + self.COM2)

    #选择魔方六面
    def onChoice(self,event):
        choice=event.GetEventObject()
        self.index=choice.GetSelection()
        if self.cap:
            if self.cap.isOpened()==False:
                self.cap.open(self.COM2)

    #拍照保存按钮点击事件
    def onCamera(self, event):
        ret, frame = self.cap.read()
        if ret:
            cv2.imwrite(self.names[self.index],frame)
            self.cap.release()
            print(self.names[self.index]+"保存成功")

    #魔方还原按钮点击事件
    def onCube(self, event):
        if self.COM1!="":
            try:
                cd.analyzeCube(self.COM1)
                if self.cap!=None:
                    self.cap.release()
            except Exception as e:
                wx.MessageDialog(self, "解析魔方出错,请检查文件是否存在或串口是否正确").ShowModal()
                print("解析魔方出错(COM1=%s,COM2=%s)"% (self.COM1,self.COM2))
                print(str(e))

    #监听摄像头事件
    def onIDLE(self,event):
        if self.cap:
            ret,frame=self.cap.read()
            if ret:
                #cv2.imshow("img",frame)
                self.showImage(frame)

    #绘制图像
    def showImage(self,img):
        height,width=img.shape[:2]
        (B,G,R)=cv2.split(img)
        bitmap=wx.Bitmap.FromBuffer(width,height,cv2.merge((R,G,B)))
        self.pic.SetBitmap(bitmap)


