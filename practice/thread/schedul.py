# coding:UTF-8
import threading, time, datetime, sched # 导入相关模块

def event_handle(schedule):
    print("[%s] lixt %s" % (threading.current_thread().name, datetime.datetime.now())) # 获取线程信息
    schedule.enter(delay=1, priority=0, action=event_handle, argument=(schedule,)) # 延迟1s继续执行

def main():
    schedule = sched.scheduler() # 创建定时调度
    schedule.enter(delay=0, priority=0, action=event_handle, argument=(schedule,))
    schedule.run() # 启动定时调度线程

if __name__ == '__main__':
    main()
