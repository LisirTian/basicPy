# 需要有 __init__.py
# 需要有setup()
# 打包成whl文件
from setuptools import setup, find_packages

setup(
    name="package_py",
    version="0.1",
    author="lixiutian",
    packages = find_packages("pub"), #模块的保存目录
    package_dir = {"":"pub"}, # 告诉setuptools包都在com下
    package_data={
        #包含的文件
        "":["*.txt","*.info","*.properties","*.py"],
        "":["data/*.*"]
    },
    exclude=["*.test","*.test.*","test.*","test"] #取消所有测试包

)