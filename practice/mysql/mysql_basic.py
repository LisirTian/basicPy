import traceback, pymysql # pip install pymysql

SQL = "SELECT  * from wms_area"
def main():
    try:
        conn = pymysql.connect(host="localhost", port=3306, charset="UTF8",user="root", password="root", database="hans_apc_wms")
        print("mysql连接成功, 当前数据库版本为: %s" % conn.get_server_info())
        print("mysql连接成功, 事务提交模式为: %s" % conn.get_autocommit())
        cmd = conn.cursor()
        cmd.execute(SQL)
        conn.commit()
        print(cmd.rowcount)
        print(cmd.arraysize)
        for item in cmd.fetchall():
            print(item)

    except Exception:
        print(traceback.format_exc())
    finally:
        conn.close()


if __name__ == '__main__':
    main()