# pip install -U openai

import openai

# 填写注册OpenAI接口账号时获取的 OpenAI API Key
openai.api_key = "OPENAI_API_KEY"

# 提问
prompt = '你是谁？'

# 访问OpenAI接口
response = openai.ChatCompletion.create(
    model='gpt-3.5-turbo',
    messages=[
        {"role": "system", "content": "你是潘高的私人智能助手。"},
        {"role": "user", "content": "谁赢得了2020年的世界职业棒球大赛?"},
        {"role": "assistant", "content": "洛杉矶道奇队在2020年赢得了世界职业棒球大赛冠军。"},
        {"role": "user", "content": prompt}
    ]
)

# 返回信息
resText = response.choices[0].message.content

print(resText)