import json
import xlwings as xw

# 将 json 文件读取成字符串

filestr = open('E:/hansProject/vueWorkspace/fms_bs/src/config/i18n/zh_CN.js',encoding="UTF-8").read()
json_data = filestr.replace('export default', '').replace("'", '"')

print(json_data)
# 对json数据解码
data = json.loads(json_data)
# data 的类型是 字典dict
print(type(data))
# 直接打印 data
# print(data)
# 遍历字典
for k, v in data.items():
    print(k + ':' + str(v))

def xlsx1():
    app = xw.App(visible=False, add_book=True)
    # wb = app.books.add()
    wb = app.books.open("E:\\tmp\\i18n.xlsx")
    # wb = xw.Book("D:\\timeFolder\\202111\\数据表\\WMS字段.xlsx")
    print('sss')
    sheets = wb.sheets
    for sheet in sheets:
        print(sheet.name)

        for row in range(1,20):
            row_str = str(row)
            print(sheet.range('A' + row_str).value)
            # print(sheet.range('C' + row_str).value)
            # print(sheet.range('D' + row_str).value)


if __name__ == '__main__':
    xlsx1()