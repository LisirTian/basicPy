import xlwings as xw
import  pandas as pd

import os,time

def xls2sql(xlsFile,sqlFile, item_code):
    # app=xw.App(visible=True,add_book=False)
    # wb = xw.Book(xlsFile)
    # # print('sss')
    # sheets = wb.sheets
    # sheet = sheets[0]
    # print(sheet.name)
    data = pd.read_excel(xlsFile)
    print(data.shape[0])
    with open(sqlFile, "w", encoding="UTF-8") as file:

        for row in range(0,data.shape[0]):
            row_str = str(row)
            a = data.iat[row,0]
            b = data.iat[row,1]
            d = data.iat[row,3]

            v1 = data.iat[row,6]
            v2 = data.iat[row,7]
            v3 = data.iat[row,8]
            v4 = data.iat[row,9]
            v5 = data.iat[row,10]

            o = data.iat[row,14]
            # print(a,b,d,v4,v5,o)

            # a = sheet.range('A' + row_str).value
            # b = sheet.range('B' + row_str).value
            # d = sheet.range('D' + row_str).value
            #
            # v1 = sheet.range('G' + row_str).value
            # v2 = sheet.range('H' + row_str).value
            # v3 = sheet.range('I' + row_str).value
            # v4 = sheet.range('J' + row_str).value
            # v5 = sheet.range('K' + row_str).value
            #
            # o = sheet.range('O' + row_str).value



            sql1 = \
                "INSERT INTO spc_item_sample " \
                  "( order_id, order_code, model_code, item_code, value, detail_id, log_time) " \
                "VALUES " \
                "((SELECT id FROM product_order WHERE product_order_code='%s')," \
                   " '%s', '%s', '%s', %f, null, '%s'" \
                "); \n"\
                   % (b,b,d,item_code,v1,o)
            sql2 = "INSERT INTO spc_item_sample " \
                   "( order_id, order_code, model_code, item_code, value, detail_id, log_time) " \
                   "VALUES " \
                   "((SELECT id FROM product_order WHERE product_order_code='%s')," \
                   " '%s', '%s', '%s', %f, null, '%s'" \
                   "); \n" \
                   % (b,b,d,item_code,v2,o)
            sql3 = "INSERT INTO spc_item_sample " \
                   "( order_id, order_code, model_code, item_code, value, detail_id, log_time) " \
                   "VALUES " \
                   "((SELECT id FROM product_order WHERE product_order_code='%s')," \
                   " '%s', '%s', '%s', %f, null, '%s'" \
                   "); \n" \
                   % (b,b,d,item_code,v3,o)
            sql4 = "INSERT INTO spc_item_sample " \
                   "( order_id, order_code, model_code, item_code, value, detail_id, log_time) " \
                   "VALUES " \
                   "((SELECT id FROM product_order WHERE product_order_code='%s')," \
                   " '%s', '%s', '%s', %f, null, '%s'" \
                   "); \n" \
                   % (b,b,d,item_code,v4,o)
            sql5 = "INSERT INTO spc_item_sample " \
                   "( order_id, order_code, model_code, item_code, value, detail_id, log_time) " \
                   "VALUES " \
                   "((SELECT id FROM product_order WHERE product_order_code='%s')," \
                   " '%s', '%s', '%s', %f, null, '%s'" \
                   ");\n " \
                   % (b,b,d,item_code,v5,o)

            # print(sql1)
            # print(sql2)
            # print(sql3)
            # print(sql4)
            # print(sql5)

            file.write(sql1)
            file.write(sql2)
            file.write(sql3)
            file.write(sql4)
            file.write(sql5)
            print("文件名: %s" % file.name)
            print("文件是否已关闭: %s" % file.closed)

    # wb.close()

def diya():
    path = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\校准\\低压\\"
    sqlPath= "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\sql\\"
    xlsFile = path+"高温温度阻值 35.xls"
    sqlFile = sqlPath+"高温温度阻值 35.sql"
    item_code= "gwzzl"
    gwzzl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)
    xlsFile = path+"常温温度阻值 35.xls"
    sqlFile = sqlPath+"常温温度阻值 35.sql"
    item_code= "cwzzl"
    cwzzl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)
    xlsFile = path+"高温校准-零压 35.xls"
    sqlFile = sqlPath+"高温校准-零压 35.sql"
    item_code= "gwjzlyl"
    gwjzlyl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)
    xlsFile = path+"常温校准-零压 35.xls"
    sqlFile = sqlPath+"常温校准-零压 35.sql"
    item_code= "cwjzlyl"
    cwjzlyl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    fileList = [gwzzl,cwzzl,gwjzlyl,cwjzlyl]
    for spc in fileList:
        xls2sql(spc["xlsFile"],spc["sqlFile"],spc["item_code"])
        # time.sleep(1)
def gaoya():
    path = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\校准\\高压\\"
    sqlPath= "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\sql\\"

    xlsFile = path+"常温温度阻值 36.xls"
    sqlFile = sqlPath+"常温温度阻值 36.sql"
    item_code= "cwzzh"
    cwzzh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path+"常温校准-零压 36.xls"
    sqlFile = sqlPath+"常温校准-零压 36.sql"
    item_code= "cwjzlyh"
    cwjzlyh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path+"高温温度阻值 36.xls"
    sqlFile = sqlPath+"高温温度阻值 36.sql"
    item_code= "gwzzh"
    gwzzh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path+"高温校准-零压 36.xls"
    sqlFile = sqlPath+"高温校准-零压 36.sql"
    item_code= "gwjzlyh"
    gwjzlyh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    fileList = [cwzzh,cwjzlyh,gwzzh,gwjzlyh]
    for spc in fileList:
        xls2sql(spc["xlsFile"],spc["sqlFile"],spc["item_code"])
        # time.sleep(1)
def zhendong():
    path = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\振动\\低压\\"
    path2 = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\振动\\高压\\"
    sqlPath= "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\sql\\"

    xlsFile = path+"振动零压.xls"
    sqlFile = sqlPath+"振动零压l.sql"
    item_code= "zdlyl"
    zdlyl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path2+"振动零压36.xls"
    sqlFile = sqlPath+"振动零压h.sql"
    item_code= "zdlyh"
    zdlyh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    fileList = [zdlyl,zdlyh]
    for spc in fileList:
        xls2sql(spc["xlsFile"],spc["sqlFile"],spc["item_code"])
        # time.sleep(1)

def maoya():
    path1 = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\低压-导热硅脂重量&铆接高度\\"
    path2 = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\振动\\高压\\"
    sqlPath= "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\sql\\"

    xlsFile = path1 + "铆接高度 数据20240108---低压.xls"
    sqlFile = sqlPath + "铆接高度 数据20240108---低压.sql"
    item_code= "mygddy"
    mygddy = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path1+"铆接高度 数据20240108----高压.xls"
    sqlFile = sqlPath+"铆接高度 数据20240108----高压.sql"
    item_code= "mygdgy"
    mygdgy = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    fileList = [mygddy,mygdgy]
    for spc in fileList:
        xls2sql(spc["xlsFile"],spc["sqlFile"],spc["item_code"])

def daoreGuizhi():
    path1 = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\低压-导热硅脂重量&铆接高度\\"
    path2 = "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\振动\\高压\\"
    sqlPath= "E:\\hansProject\\hansFile\\安培龙\\开发\\SPC\\sql\\"

    xlsFile = path1 + "导热硅脂重量 数据20240108--低压.xls"
    sqlFile = sqlPath + "导热硅脂重量 数据20240108--低压.sql"
    item_code= "drgzl"
    drgzl = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    xlsFile = path1+"导热硅脂重量 数据20240108--高压.xls"
    sqlFile = sqlPath+"导热硅脂重量 数据20240108--高压.sql"
    item_code= "drgzh"
    drgzh = dict(xlsFile=xlsFile, sqlFile = sqlFile, item_code =item_code)

    fileList = [drgzl,drgzh]
    for spc in fileList:
        xls2sql(spc["xlsFile"],spc["sqlFile"],spc["item_code"])

def main():
    # diya()
    # gaoya()
    # zhendong()
    maoya()
    # daoreGuizhi()
if __name__ == '__main__':
    main()