# https://blog.51cto.com/liangchaoxi/5687269
# pip install paho-mqtt
import paho.mqtt.client as mqtt
import json
import time
import os

HOST = "192.168.43.17"
PORT = 1883
client_id = "1083421xxxxx"  # 没有就不写，此处部分内容用xxx代替原内容，下同


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("data/receive")  # 订阅消息


def on_message(client, userdata, msg):
    print("主题:" + msg.topic + " 消息:" + str(msg.payload.decode('utf-8')))


def on_subscribe(client, userdata, mid, granted_qos):
    print("On Subscribed: qos = %d" % granted_qos)


def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection %s" % rc)


data = {
    "type": 1,
    "timestamp": time.time(),
    "messageId": "9fcda359-89f5-4933-3324",
    "command": "xx/recommend",
    "data": {
        "openId": "xxxx",
        "appId": "mes_pub",
        "recommendType": "temRecommend"
    }
}
# param = json.dumps(data)
# client = mqtt.Client(client_id)
# client.username_pw_set("xxxxxx", "xxxxxx")
# client.on_connect = on_connect
# client.on_message = on_message
# client.on_subscribe = on_subscribe
# client.on_disconnect = on_disconnect
# client.connect(HOST, PORT, 60)
# client.publish("data/send", payload=param, qos=0)     # 发送消息
# client.loop_forever()

dirPath = "E:\\hansProject\\hansFile\\安培龙\\开发\\test"


def test():
    client = mqtt.Client(protocol=mqtt.MQTTv5)
    client.connect(HOST, PORT, 60)
    client.max_inflight_messages_set(0)

    with open(dirPath + "\\mqttOrd47.txt", "r", encoding="UTF-8") as file:
        i = 0
        for line in file:
            # print(line)
            print(line, end="\n")
            # for i in range(0,30):
            i = i + 1
            time.sleep(1.1)
            info = client.publish("scada/up", line, 1)  # 发布一个主题为'chat',内容为‘hello liefyuan’的信息
            print(info)
            # if i>100:
            #     break


    # client.loop_forever()


def main():
    print("")


if __name__ == '__main__':
    test()
