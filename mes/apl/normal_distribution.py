import xlwings as xw
import numpy as np

def xlsx1():
    list=[]
    wb = xw.Book("E:/TencentFiles/WeChat Files/l936108248/FileStorage\File/2023-07/13.CPK铆接高度.xlsx")
    sheets = wb.sheets
    for sheet in sheets:
        print(sheet.name)
        if sheet.name != "X-R":
            break
        for row in range(10,15):
            # for col in range(67,91)\
            for colStr in ('C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA'):
                row_str = str(row)
                # colStr = bytes([col]).decode('utf-8')
                # print(colStr , end="','")
                val = sheet.range(colStr + row_str).value
                # print(val, end=',')
                list.append(val)
            # print("\n")

    return list

# print(bytes([65]).decode('utf-8'))

# μ表示均值，σ表示标准差

def main():
    list = xlsx1()
    print(list)
    ave = np.mean(list)
    std = np.std(list,ddof=0)
    print(ave)
    print(std)

if __name__ == '__main__':
    main()